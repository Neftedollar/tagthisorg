﻿namespace Business
open Data.Types
module Queries =
type Queries(connection:LazyConnection)=
   member val Connection = connection
  
type IBookmarksQueries =
   abstract member init:unit -> unit


type BookmarksQueries(connection) =
   inherit Queries(connection)

