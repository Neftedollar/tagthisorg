﻿
var tagthisorg = angular.module('tagthisorg', ['infinite-scroll', 'ui.router']);
tagthisorg.directive("passwordVerify", function () {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;

                if (scope.passwordVerify || ctrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordVerify", false);
                            return viewValue;
                        } else {
                            ctrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});
tagthisorg.factory('request', ['$http', function ($http) {

    return {
        request: function (method, url, data, okCallback, koCallback) {
            $http({
                method: method,
                url: url,
                data: data
            }).success(okCallback).error(koCallback);
        },
        authentifiedRequest: function (method, url, data, okCallback, koCallback) {
            $http({
                method: method,
                url: url,
                data: data,
                headers: { '__RequestVerificationToken': encodeURIComponent($(document.querySelector('#antiForgeryToken')).val()) }
            }).success(okCallback).error(koCallback);
        },

        csrfTokenRequest: function (method, url, data, okCallback, koCallback) {
            $http({
                method: method,
                url: url,
                data: data,
                headers: { 'X-XSRF-Token': $(document.querySelector("input[name='__RequestVerificationToken']")).val() }
            }).success(okCallback).error(koCallback);
        }
    };
}]);
tagthisorg.config(['$routeProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', function ($routeProvider, $locationProvider, $stateProvider, $urlRouterProvider) {
    //$routeProvider
    //.when('/index', { templateUrl: "/Home/Home", controller: IndexController })
    //.when('/search', { templateUrl: "/Home/Search", controller: IndexController })
    //.when('/manage', { templateUrl: "/Account/Manage", controller: ManageController })
    //.when('/tags', { templateUrl: "/Home/Home", controller: TagsController })     ///Account/Manage
    //.otherwise({ redirectTo: '/index' })
    //$locationProvider.hashPrefix = '!';
    $stateProvider
    .state('index', {
        url: '/index?type',
        templateUrl: "/Home/Home", controller: 'IndexController'
    })
        .state('search', {
            url: '/search?tags?type',
            templateUrl: "/Home/Search", controller: 'IndexController'
        });
    $urlRouterProvider.otherwise('/index');
}]);
tagthisorg.directive('ngBlur', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngBlur']);
        element.bind('blur', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);
tagthisorg.directive('ngFocus', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngFocus']);
        element.bind('focus', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);

tagthisorg.factory('$searchType', function () {
    return {
        searchTypes: [{ id: 0, type: "own", title: "мои закладки" }, { id: 1, type: "general", title: "база TagThis.Org" }],
        selected: { type: "general", title: "база TagThis.Org" }
    }
});