namespace FsWeb

open System
open System.Web
open System.Web.Mvc
open System.Web.Routing
open System.Web.Http
open System.Data.Entity
open System.Web.Optimization
open System.IO
open StructureMap
open System.Web.Http
open Microsoft.Web.WebPages.OAuth
open Newtonsoft.Json.Serialization
open System.Web.Http;
open AttributeRouting.Web.Http.WebHost;
open FsWeb.Models
open System.Web.Http.Controllers
open System.Net.Http
open System.Web.Http.Dispatcher
//open Business.Context

type BundleConfig() =
    static member RegisterBundles (bundles:BundleCollection) =
          printf "hi"
//        bundles.Add(ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-1.*"))
//
//        bundles.Add(ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui*"))
//
//        bundles.Add(ScriptBundle("~/bundles/jqueryval").Include(
//                                     "~/Scripts/jquery.unobtrusive*",
//                                     "~/Scripts/jquery.validate*"))
//            
//        bundles.Add(ScriptBundle("~/bundles/extLibs").Include(
//                                     "~/Scripts/underscore.js",   
//                                     "~/Scripts/toastr.js",
//                                     "~/Scripts/angular.js"))
//
//        bundles.Add(ScriptBundle("~/bundles/localApp").Include(
//                                 "~/Scripts/app/main.js",
//                                 "~/Scripts/app/utility.js",
//                                 "~/Scripts/controllers/*.js",
//                                 "~/Scripts/app/router.js"))
//
//        bundles.Add(ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"))
//
//        bundles.Add(StyleBundle("~/Content/css").Include("~/Content/*.css"))
//
//        bundles.Add(StyleBundle("~/Content/themes/base/css").Include(
//                                    "~/Content/themes/base/jquery.ui.core.css",
//                                    "~/Content/themes/base/jquery.ui.resizable.css",
//                                    "~/Content/themes/base/jquery.ui.selectable.css",
//                                    "~/Content/themes/base/jquery.ui.accordion.css",
//                                    "~/Content/themes/base/jquery.ui.autocomplete.css",
//                                    "~/Content/themes/base/jquery.ui.button.css",
//                                    "~/Content/themes/base/jquery.ui.dialog.css",
//                                    "~/Content/themes/base/jquery.ui.slider.css",
//                                    "~/Content/themes/base/jquery.ui.tabs.css",
//                                    "~/Content/themes/base/jquery.ui.datepicker.css",
//                                    "~/Content/themes/base/jquery.ui.progressbar.css",
//                                    "~/Content/themes/base/jquery.ui.theme.css"))
///for ApiControllers
type ServiceActivator(container : IContainer) = // ���
   interface IHttpControllerActivator with
       member this.Create(request: HttpRequestMessage, controllerDescriptor:HttpControllerDescriptor , controllerType:Type) = 
          container.GetInstance(controllerType) :?> IHttpController
              //container.

       
type Route = { controller : string; action : string; id : UrlParameter }
type ApiRoute = { id : RouteParameter }
type Global() as self =
    inherit System.Web.HttpApplication() 
    do 
       self.EndRequest.Add( fun e -> ObjectFactory.ReleaseAndDisposeAllHttpScopedObjects());
       self.BeginRequest.Add(fun e ->
              System.Diagnostics.Debug.Write(e);
               )


    [<DefaultValue>]  static  val  mutable private _container:IContainer;

    static member Container with get():IContainer = Global._container and set(v) = Global._container <- v
    static member RegisterGlobalFilters (filters:GlobalFilterCollection) =
        filters.Add(new HandleErrorAttribute())

    static member RegisterRoutes(routes:RouteCollection) =
        routes.IgnoreRoute( "{resource}.axd/{*pathInfo}" )
//        routes.MapHttpRoute( "DefaultApi", "api/{controller}/{id}", 
//            { id = RouteParameter.Optional } ) |> ignore
        
        routes.MapRoute("Default", "{controller}/{action}/{id}", 
            { controller = "Home"; action = "Index"; id = UrlParameter.Optional } ) |> ignore
    static member RegisterWebApi (config:HttpConfiguration) = 
        config.Routes.MapHttpAttributeRoutes()// fun (cfg:HttpWebConfiguration) ->  cfg.InlineRouteConstraints.Add("string", typeof<Bookmark>)
//        config.EnableQuerySupport();
        
               // config.Routes.MapHttpAttributeRoutes(fun (c:HttpWebConfiguration) -> c.AddRoutesFromAssemblyOf<Global>())
        
    static member RegisterOAuthLogin() = 
            OAuthWebSecurity.RegisterMicrosoftClient("000000004410F028","auT3dg8Lrm4tmS8ZodpbEeUHyfirPNY6");
            OAuthWebSecurity.RegisterTwitterClient("CEv8BndmwktKAZzoqZ0YcQ","hQZXIkRBC8Qx1vymqUwyE12LZTKiOEmGP4r0T5I");
            OAuthWebSecurity.RegisterFacebookClient("515651185115491","769ec0e9f4f59131b458837dbdb2a3eb");
            OAuthWebSecurity.RegisterGoogleClient();

    static member RegisterAttributeRoutes(routes:HttpRouteCollection) =
        routes.MapHttpAttributeRoutes();
    member this.Start(connectionString:string) =
        let container:IContainer = DependencyResolution.IoC.Initialize(connectionString);
        //System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseAlways<Data.DataContext>());
        Global._container <-container;
        //container.
        DependencyResolver.SetResolver(new DependencyResolution.StructureMapDependencyResolver(container));
        AreaRegistration.RegisterAllAreas()
        Global.RegisterWebApi GlobalConfiguration.Configuration
        GlobalConfiguration.Configuration.Services.Replace(typeof<IHttpControllerActivator>, ServiceActivator(container)) //���
        Global.RegisterRoutes RouteTable.Routes
        Global.RegisterGlobalFilters GlobalFilters.Filters
        Global.RegisterOAuthLogin()

        //show db
//        Global.RegisterAttributeRoutes GlobalConfiguration.Configuration.Routes
        //BundleConfig.RegisterBundles BundleTable.Bundles 