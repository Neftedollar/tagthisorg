﻿namespace FsWeb.Models

open System;
open System.Collections.Generic;
open System.ComponentModel.DataAnnotations;
open System.Linq;
open System.Web;

type LoginModel() =
   [<Required>]
   member val UserName = "" with get, set
   [<Required>]
   [<DataType(DataType.Password)>]
   member val Password = "" with get, set
   member val RememberMe = false with get,set

type RegisterModel() =
    [<Required>]
    member val UserName = "" with get,set
    [<Required>]
    [<StringLength(100, ErrorMessage = "Пароль должен содержать минимум {2} символов. Простите за это.", MinimumLength = 6)>]
    [<DataType(DataType.Password)>]
    member val Password = "" with get, set

    [<Required>]
    [<Compare("Password", ErrorMessage = "Подтверждение пароля должно совпадать с паролем. Иначе это не подвтеждение.")>]
    [<DataType(DataType.Password)>]
    member val ConfirmPassword = "" with get, set
    member val RememberMe = false with get, set
type LocalPasswordModel() =
    [<Required>]
    [<DataType(DataType.Password)>]
    member val OldPassword = "" with get, set

    [<Required>]
    [<DataType(DataType.Password)>]
    member val NewPassword = "" with get, set

    [<Required>]
    [<Compare("NewPassword", ErrorMessage = "Новый пароль и его подвтерждение не совпадают.")>]
    member val ConfirmPassword = "" with get, set

type RegisterExternalLoginModel() =
        [<Required>]
        //[<Display(Name = "User name")>]
        member val  UserName = ""  with get, set
        member val ExternalLoginData = "" with get, set
        member val Email = "" with get, set

type ExternalLogin() =
        member val Provider = "" with get,set
        member val ProviderDisplayName = "" with  get, set
        member val ProviderUserId = "" with get, set
        //public string ExternalLoginData { get; set; }
//[Required]
//        [Display(Name = "User name")]
//        public string UserName { get; set; }
//
//        [Required]
//        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
//        [DataType(DataType.Password)]
//        [Display(Name = "Password")]
//        public string Password { get; set; }
//
//        [DataType(DataType.Password)]
//        [Display(Name = "Confirm password")]
//        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
//        public string ConfirmPassword { get; set; }

