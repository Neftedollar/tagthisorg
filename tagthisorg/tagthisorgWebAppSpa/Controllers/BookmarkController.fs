﻿namespace FsWeb.Controllers

open System.Collections.Generic
open System.Web
open System.Web.Mvc
open System.Net.Http
open System.Web.Http
open AttributeRouting;
open AttributeRouting.Web;
open AttributeRouting.Web.Http
open System.Diagnostics
open System
open FsWeb.Models
open FsWeb.Utils
open Business;
open Data.ViewModels
open WebMatrix.WebData

[<RoutePrefix("/api/Bookmark")>]
type BookmarkController(bc:BusinessDomain.IBusinessContext) =
 inherit ApiController()

  [<POST("Create");HttpPost>]
  //[<HttpPost>]
  member this.Create(b: Bookmark) = 
     if this.ModelState.IsValid then
         try

           if this.User.Identity.IsAuthenticated then
             let bk = bc.AddBookmarks (b , this.User.Identity.Name) |> Async.RunSynchronously
             let SaveBookamrks =  Async.RunSynchronously <| bc.BookmarksContext.SetBookmarkToUser WebSecurity.CurrentUserId ( bk |> Array.map ( fun b -> b.Id ) )
             this.Request.CreateResponse(Net.HttpStatusCode.OK);
           else 
              bc.AddBookmarks b |> Async.RunSynchronously |> ignore
              this.Request.CreateResponse(Net.HttpStatusCode.OK);
         with
         |e ->  this.Request.CreateErrorResponse(Net.HttpStatusCode.InternalServerError,e);
     else
         this.Request.CreateErrorResponse(Net.HttpStatusCode.InternalServerError, Exception("Вы ввели не верные данные"));
     //success "Ok"