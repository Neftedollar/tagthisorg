﻿namespace FsWeb.Controllers
open System.Collections.Generic
open System.Web
open System.Web.Mvc
open System.Net.Http
open System.Web.Http
open AttributeRouting;
open AttributeRouting.Web;
open AttributeRouting.Web.Http
open System.Diagnostics
open System
open FsWeb.Models
open FsWeb.Utils
open Business
open Business.BusinessDomain

[<RoutePrefix("/api/Tag")>]
type TagController(bc:IBusinessContext) =
 inherit ApiController()

  [<GET("Autocomplete/{q:alpha}?{user:bool}&{page:int}");HttpGet>]
 member x.Autocomplete q user page =  ""// bc.TagContext.