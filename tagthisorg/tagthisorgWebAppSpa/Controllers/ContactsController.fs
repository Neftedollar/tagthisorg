﻿namespace FsWeb.Controllers

open System.Collections.Generic
open System.Web
open System.Web.Mvc
open System.Net.Http
open System.Web.Http
open AttributeRouting;
open AttributeRouting.Web;
open AttributeRouting.Web.Http
open FsWeb.Models

[<RoutePrefix("/api/Contact")>]
type ContactsController() =
    inherit ApiController()
    //http://www.asp.net/web-api/overview/odata-support-in-aspnet-web-api/odata-security-guidance
    // This is for demonstration purposes only. 
    let contacts = seq { yield Contact(FirstName = "John", LastName = "Doe", Phone = "123-123-1233")
                         yield Contact(FirstName = "Jane", LastName = "Doe", Phone = "123-111-9876") 
                         yield Contact(FirstName = "a", LastName = "q", Phone = "123-111-9876")
                         yield Contact(FirstName = "s", LastName = "w", Phone = "123-111-9876")
                         yield Contact(FirstName = "d", LastName = "e", Phone = "123-111-9876")
                         yield Contact(FirstName = "f", LastName = "r", Phone = "123-111-9876")
                         yield Contact(FirstName = "g", LastName = "t", Phone = "123-111-9876")
                         yield Contact(FirstName = "h", LastName = "y", Phone = "123-111-9876")}

    // GET /api/contacts
    //[<GET("")>]
//    [<Queryable(PageSize=3);HttpGet;GET("")>] //api/contacts?$skip=1
    member x.Get() = 
        // TODO: Replace with your code to retrieve the contacts list
        contacts
    // POST /api/contacts
    //[<GET("{}")>]
    member x.Post ([<FromBody>] contact:Contact) = 
        // TODO: Replace with your code to persiste the contact information
        contacts |> Seq.append [ contact ] 
    [<GET("{id:alpha}/{ln:alpha}") ; HttpGet>]
    member x.FuckIt (id:string) (ln:string) =
      let cntcs =  seq {
        for c in contacts do
                if c.FirstName = id then yield c}
      cntcs |> Seq.map (      
        match ln with
            |"fs" -> fun c -> c.FirstName
            |"ls" -> fun c -> c.Phone
            |_ -> fun c -> c.Phone
      )
    [<GET("hi");HttpGet>]
    member this.hi() = 
        "hi"
            
