﻿namespace FsWeb.Controllers


open System.Web
open System.Web.Mvc
//open NewBusiness;
open Business
open FsWeb.Models
//open Business.Context
//[<HandleError>]
[<FsWeb.Filters.InitializeSimpleMembership>]
type HomeController(bc: Business.BusinessDomain.IBusinessContext) =
    inherit Controller()
    
    member this.Index () =
           //show db
           //debug( newUrl.Command.ToString())
           let kk = bc.BookmarksContext;
           this.View() :> ActionResult
    member this.Home() =
           let q = this;
           this.PartialView("Home");
    member this.Search() =
           this.PartialView("Search");
