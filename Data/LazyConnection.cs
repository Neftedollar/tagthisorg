﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Types
{
    public class LazyConnection : Lazy<IDbConnection>, IDisposable
    {
        public LazyConnection(string connectionString)
            : base(() =>
            {
                var c = new SqlConnection(connectionString);
                c.Open();

                return c;
            })
        {
        }

        public void Dispose()
        {
            if (IsValueCreated)
            {
                Value.Dispose();
            }
        }
    } 
}
