﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ViewModels
{
    public class Bookmark
    {
        //[Required]
        //[Url(ErrorMessage = "Need Email")]
        public string url { get; set; }
        //[Required]
        public string[] tags { get; set; }
        public string title { get; set; }
    }
    public class Tag
    {
        public long Id { get; set; }
        public string tag { get; set; }
    }
    public class UriVM
    {
        public long Id {get;set;}
        public string Uri { get; set; }
        public string HostUri { get; set; }
        public string favicon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool isFile { get; set; }
        public string Image { get; set; }
        public int position { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
    }
}
